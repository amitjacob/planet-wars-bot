import math

def distance(a, b):
	return math.sqrt( ((a.x()-b.x())**2)+((a.y()-b.y())**2) )

def minimum_distance(source, planets):
	minimum = planets[0]
	for planet in planets:
		if distance(source, planet) < distance(source, minimum):
			minimum = planet
	return minimum

def max_fleets_in_planets(planets):
	max = planets[0]
	for planet in planets:
		if planet.num_ships() > max.num_ships():
			max = planet
	return max



def do_turn(pw):
	if pw.my_fleets() == 0:
		return

	if len(pw.my_planets()) == 0:
		return

	source = max_fleets_in_planets(pw.my_planets())
	
	dest = 0
	
	if len(pw.neutral_planets()) >= 1:
		dest = minimum_distance(source, pw.neutral_planets())
	else:
		if len(pw.enemy_planets()) >= 1:
			dest = minimum_distance(source, pw.enemy_planets())

	
	num_ships = source.num_ships() / 2
	
	pw.debug('Num Ships: ' + str(num_ships))

	pw.issue_order(source, dest, num_ships)


